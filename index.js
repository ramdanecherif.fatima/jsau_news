'use strict'

let nomNouvel = [
	"economique",
	"politique",
	"sportive",
	"artistique",
	"ecologique",
	"psycologique",
	"informatique",
	"physique",
	"maths",
	"artistique"
	]

module.exports = {
	nomNouvel, validate: function(categ) {
		return nomNouvel.includes(categ);
	}
}